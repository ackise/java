package HW05;

import org.junit.jupiter.api.Test;
import HW05.Enums.Species;
import static org.junit.jupiter.api.Assertions.*;

class PetTest {

    @Test
    void testToString() {
        Pet pet = new Pet(Species.CAT,"TEST",5,50, new String[]{"annoy", "run"});
        String result = pet.toString();
        assertEquals(result,"CAT{nickname='TEST', age=5, trickLevel=50, habits=[[annoy, run]]}");

    }
}