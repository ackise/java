package HW05;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class FamilyTest {

    Family testFamily = new Family(new Human(),new Human());
    Human child1 = new Human("child","first",2020);
    Human child2 = new Human("child","second",2020);

    @BeforeEach
    void clearChildrenArray(){
        Human [] children = new Human[]{};
    }

    @Test
    void deleteChildHuman_shouldDeleteChild() {
        testFamily.setChildren(new Human[]{child1,child2});
        testFamily.deleteChild(1);
        assertArrayEquals(testFamily.getChildren(),new Human[]{child1});
    }

    @Test
    void deleteChildHuman_shouldNotDeleteChild() {
        testFamily.setChildren(new Human[]{child1,child2});
        testFamily.deleteChild(5);
        assertArrayEquals(testFamily.getChildren(),new Human[]{child1,child2});
    }

    @Test
    void addNewChild_shouldBeAdded(){
        Human child3 = new Human("child","third",2020);
        Human[] children = testFamily.getChildren();
        System.out.println(children.length);
        testFamily.addChild(child3);
        assertEquals(testFamily.getChildren().length, children.length+1);
    }

    @Test
    void addNewChild_shouldBeEqualsToAddedChild(){
        Human child3 = new Human("child","third",2020);
        Human[] children = testFamily.getChildren();
        System.out.println(children.length);
        testFamily.addChild(child3);
        assertEquals(testFamily.getChildren()[0], child3);
    }

    @Test
    void showsAmountOfRelatives(){
        Human child3 = new Human("child","third",2020);
        testFamily.addChild(child3);
        assertEquals(testFamily.countFamily(), 3);
    }
}