package HW05;

import static org.junit.jupiter.api.Assertions.*;
import HW05.Enums.DayOfWeek;



class HumanTest {

    @org.junit.jupiter.api.Test
    void testToString() {
        String[][]schedule = {
                {DayOfWeek.MONDAY.name(), "courses"},
                {DayOfWeek.SUNDAY.name(),"Cycling"}
        };
        Human human = new Human("TestName","TestSurname",2020,200,schedule);
        String result = human.toString();
        assertEquals(result, "Human{name=TestName, surname=TestSurname, year=2020, iq=200, schedule=[[[MONDAY, courses], [SUNDAY, Cycling]]}");

    }
}