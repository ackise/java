package HW01;


import java.util.Scanner;

public class RandomNumber {


    public static void print(String s){
        System.out.print(s);
    }
    static int random_int(int from, int to) {
        int count = (int) (Math.random()*(to - from +1)+from);
        return count;
    }
    static void check(int player_number, int random_number, String name){
        if(player_number > random_number){
            print("Your number is too big. Please, try again. \n");
        }
        else if(player_number < random_number){
            print("Your number is too small. Please, try again.\n");
        }
        else if(player_number == random_number){
            System.out.printf("Congratulations %s !",name);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        print("Let the game begin! \n");
        print("Input your name: \n");
        String name = in.nextLine();
        System.out.printf("Your name: %s \n", name);
        for(; ;){

            print("Input your number: ");
            int num = in.nextInt();
            int h = random_int(0,100);
            System.out.printf("Random number is : %d \n",h);

            if(num !=h){
                check(num,h,name);
            }
            if(num == h) {
                check(num,h,name);
                break;
            }
        }

    }
}

