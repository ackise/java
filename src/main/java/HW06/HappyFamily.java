package HW06;

import HW06.Enums.DayOfWeek;
import HW06.Humans.Human;
import HW06.Pets.DomesticCat;

public class HappyFamily {
    public static void main(String[] args) {
        DomesticCat Mia = new DomesticCat("mia", 2, 20, new String[]{"jump", "run"});
        String[][]schedule = {
                {DayOfWeek.MONDAY.name(), "courses"},
                {DayOfWeek.SUNDAY.name(),"Cycling"}
        };
        Human Max = new Human("Max", "Pyatochkin", 1992,200,schedule);
        Human Julia = new Human("Julia", "Julka", 1992,200,schedule);

        Family family = new Family(Max, Julia);


        Human Petya = new Human("Petrik","Pyatochkin",2020, 100, Mia, Julia, Max,schedule,family);

        family.setChildren(new Human[]{Petya});

        Mia.eat();
        Mia.respond();
        Petya.greetPet();
        System.out.println(Petya.getFather());
        System.out.println(Petya.getMother());
        System.out.println(Petya.toString());
        System.out.println(Mia.toString());

        for (int i = 0; i < 1000000; i++) {
            Family family1 = new Family(new Human(),new Human());

        }


    }

}
