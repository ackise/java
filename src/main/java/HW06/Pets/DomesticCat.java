package HW06.Pets;

import HW06.Enums.Species;

public class DomesticCat extends Pet implements Foul{


    public DomesticCat(String nickname) {
        super(nickname);
        super.species = Species.DOMESTIC_CAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void foul() {
        System.out.println("Glass destroyed");
    }

    @Override
    public void respond() {
        System.out.println("Hi,owner! . I'm - cat " + super.nickname + ". i missed you!");

    }
}
