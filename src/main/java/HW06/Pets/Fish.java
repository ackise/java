package HW06.Pets;

import HW06.Enums.Species;

public class Fish extends Pet{

    public Fish(String nickname) {
        super(nickname);
        super.species = Species.FISH;
    }

    @Override
    public void respond() {
        System.out.println("Hi,owner! . I'm - fish" + super.nickname + ". i missed you!");
    }
}
