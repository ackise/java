package HW06.Pets;

import HW06.Enums.Species;

public class RoboCat extends Pet implements Foul{

    public RoboCat(String nickname) {
        super(nickname);
        super.species = Species.ROBO_CAT;
    }
    @Override
    public void foul() {
        System.out.println("DESTROY ALL HUMANS!");
    }

    @Override
    public void respond() {
        System.out.println("Hi,owner! . I'm - robo_cat " + super.nickname + ". i will destroy you!");

    }

    public RoboCat( String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }
}
