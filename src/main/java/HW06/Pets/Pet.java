package HW06.Pets;

import HW06.Enums.Species;

import java.util.Arrays;
import java.util.Objects;

public abstract class Pet {
    public String nickname;
    protected Species species;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(String nickname) {
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public Pet (){}

    public void eat(){
        System.out.println("I'm eating");
    }

    public abstract void respond();

    @Override
    public boolean equals(Object pet) {

        if (pet instanceof Pet) {
            Pet newPet = (Pet) pet;
            boolean nullNames = this.nickname == null && newPet.nickname == null;

            return (nullNames || (this.nickname != null && this.nickname.equals(newPet.nickname))) && this.age == newPet.age;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nickname, age);
    }

    @Override
    public String toString() {
        return species + "{nickname='" + this.nickname + "', age=" + this.age + ", trickLevel=" + this.trickLevel + ", habits=[" + Arrays.toString(this.habits) + "]}";
    }

    @Override
    protected void finalize() {
        System.out.println(this);
    }








}
