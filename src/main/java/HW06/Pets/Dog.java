package HW06.Pets;

import HW06.Enums.Species;

public class Dog extends Pet implements Foul{

    public Dog(String nickname) {
        super(nickname);
        super.species = Species.DOG;
    }

    @Override
    public void foul() {
        System.out.println("Flat was destroyed");
    }

    public Dog(String nickname, int age, int trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println("Hi,owner! . I'm - dog " + super.nickname + ". i missed you!");
    }

}
