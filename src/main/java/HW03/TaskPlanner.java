package HW03;

import java.util.Scanner;

public class TaskPlanner {


    public static void print(String s){
        System.out.print(s);
    }
    public static void result (String format,Object ... args){
        System.out.printf(format,args);
    }


    public static String search(String [][] arr, String day) {
        String task = null;
        try{
            switch (day){
                case "monday":
                    task = arr[0][1];
                    result("Your task for %s is - %s",day,task) ;
                    break;
                case "tuesday":
                    task = arr[1][1];
                    result("Your task for %s is - %s",day,task) ;

                    break;
                case  "wednesday":
                    task = arr[2][1];
                    result("Your task for %s is - %s",day,task) ;

                    break;
                case  "thursday":
                    task = arr[3][1];
                    result("Your task for %s is - %s",day,task) ;

                    break;
                case "friday":
                    task = arr[4][1];
                    result("Your task for %s is - %s",day,task) ;

                    break;
                case  "saturday":
                    task = arr[5][1];
                    result("Your task for %s is - %s",day,task) ;

                    break;
                case "sunday":
                    task = arr[6][1];
                    result("Your task for %s is - %s",day,task) ;

                    break;
                case "exit": return "exit";
                default: throw new Exception();

            }

        } catch (Exception e) {
            System.out.println("Sorry, I don't understand you, please try again.");
        }
    return task;
    }

    public static void main(String[] args) {

        String [][]schedule = {
                {"monday","do homework",},
                {"tuesday","go to gym"},
                {"wednesday","go to courses"},
                {"thursday","go to cinema"},
                {"friday","go to courses"},
                {"saturday","clean my flat"},
                {"sunday","watch films"},

        };
        Scanner in = new Scanner(System.in);

        for(; ;){

            print("\n Please, input the day of the week:  ");
            String  day = in.next().toLowerCase();
            String task = search(schedule,day);


            if( task == "exit") break;


        }

    }
}
