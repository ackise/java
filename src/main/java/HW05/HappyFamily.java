package HW05;

import HW05.Enums.DayOfWeek;
import HW05.Enums.Species;

public class HappyFamily {
    public static void main(String[] args) {
        new Pet();
        String[][]schedule = {
                {DayOfWeek.MONDAY.name(), "courses"},
                {DayOfWeek.SUNDAY.name(),"Cycling"}
        };
        Pet cat = new Pet(Species.CAT,"mia",2,20, new String[]{"jump", "run"});
        Human Max = new Human("Max", "Pyatochkin", 1992,200,schedule);
        Human Julia = new Human("Julia", "Julka", 1992,200,schedule);

        Family family = new Family(Max, Julia);


        Human Petya = new Human("Petrik","Pyatochkin",2020, 100, cat, Julia, Max,schedule,family);

        family.setChildren(new Human[]{Petya});

        cat.eat();
        cat.respond();
        cat.foul();
        Petya.greetPet();
        System.out.println(Petya.getFather());
        System.out.println(Petya.getMother());
        System.out.println(Petya.toString());
        System.out.println(cat.toString());

//        for (int i = 0; i < 1000000; i++) {
//            Family family1 = new Family(new Human(),new Human());
//
//        }


    }

}
