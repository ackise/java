package HW05.Enums;

public enum Species {
    CAT(true,true,true,4,false),
    SNAKE(false,true,false,0,false),
    RACCOON(true,true,true,4,false),
    PARROT(false,false,true,2,true),
    FISH(false,false,false,0,false);

    boolean mammal;
    boolean teeth;
    boolean legs;
    int numberOfLegs;
    boolean wings;

    Species(boolean mammal, boolean teeth, boolean legs, int numberOfLegs, boolean wings) {
        this.mammal = mammal;
        this.teeth = teeth;
        this.legs = legs;
        this.numberOfLegs = numberOfLegs;
        this.wings = wings;
    }
}
