package HW04;

public class HappyFamily {
    public static void main(String[] args) {
        new Pet();
        String[][]schedule = {
                {"Wednesday", "courses"},
                {"Sunday","Cycling"}
        };
        Pet cat = new Pet("cat","mia",2,20, new String[]{"jump", "run"});
        Human  Max = new Human("Max", "Pyatochkin", 1992);
        Human Julia = new Human("Julia", "Julka", 1992);

        Family family = new Family(Max, Julia);


        Human Petya = new Human("Petrik","Pyatochkin",2020, 100, cat, Julia, Max,schedule,family);

        family.setChildren(new Human[]{Petya});

        cat.eat();
        cat.respond();
        cat.foul();
        Petya.greetPet();
        System.out.println(Petya.getFather());
        System.out.println(Petya.getMother());
        System.out.println(Petya.toString());


    }

}
