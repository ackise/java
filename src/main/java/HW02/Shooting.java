package HW02;

import java.util.Scanner;

public class Shooting {

    public static void print(String s){
        System.out.print(s);
    }

    static int random_int(int from, int to) {
        int count = (int) (Math.random()*(to - from +1)+from);
        return count;
    }

    static String [][] matrix(String[][] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                print("\t" + array[i][j] + "\t" + "|");
            }
            System.out.println();
        };
        return  array;
    }
    static String [][] matrix(String n, String[][] array,int row,int clm){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                array[row][clm] = n;
                print("\t" + array[i][j] + "\t" + "|");
            }
            System.out.println();
        }
        return array;
    }

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        int target_row = random_int(1,5);
        int target_clm = random_int(1,5);

//        Что бы узнать какие координаты мишени
//        System.out.printf("clm %d \n",target_clm);
//        System.out.printf("row %d \n",target_row);


        print("Let the game begin! \n");

       String [][]area = {
               {"0","1","2","3","4","5"},
               {"1","-","-","-","-","-"},
               {"2","-","-","-","-","-"},
               {"3","-","-","-","-","-"},
               {"4","-","-","-","-","-"},
               {"5","-","-","-","-","-"},

       };

       String[][] f = matrix(area);

       for(; ;){

           print("\n Input column: ");
           int clm = in.nextInt();
           print("\n Input row: ");
           int row = in.nextInt();


           if(  clm > area.length - 1){
               print("\n You are out of column range.Try again.");
           } if (row > area.length - 1){
               print("\n You are out of row range.Try again.");
           }if( row <= area.length -1 && clm <= area.length - 1 ){
               print("\n");
               String[][] g = matrix("*",area,row,clm);
           }if (target_row == row && target_clm == clm){
               print("You win! \n");
               String[][] l = matrix("X",area,row,clm);
               break;
           }

       }

    }

}
